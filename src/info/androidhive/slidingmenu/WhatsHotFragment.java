package info.androidhive.slidingmenu;

import info.androidhive.slidingmenu.adapter.MyCustomBaseAdapter;
import info.androidhive.slidingmenu.model.CountryListObject;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.patrika.pie.R;

@SuppressLint("NewApi") public class WhatsHotFragment extends Fragment {
	
//	public WhatsHotFragment(){}
	
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_whats_hot, container, false);
     ArrayList<CountryListObject> searchResults = GetSearchResults();
        
        final ListView lv1 = (ListView) rootView.findViewById(R.id.ListView01);
        lv1.setAdapter(new MyCustomBaseAdapter(getActivity(), searchResults));
        
        lv1.setOnItemClickListener(new OnItemClickListener() {
         @Override
         public void onItemClick(AdapterView<?> a, View v, int position, long id) { 
          Object o = lv1.getItemAtPosition(position);
          CountryListObject fullObject = (CountryListObject)o;
          Toast.makeText(getActivity(), "You have chosen: " + " " + fullObject.getName(), Toast.LENGTH_LONG).show();
         }  
        });
        return rootView;
    }
	
	private ArrayList<CountryListObject> GetSearchResults(){
	     ArrayList<CountryListObject> results = new ArrayList<CountryListObject>();
	     
	     CountryListObject sr1 = new CountryListObject();
	     sr1.setName("JAIPUR");
	     sr1.setCityState("pie@epatrika.com");
	     sr1.setPhone("9660016803 | 9799039333");
	     results.add(sr1);
	     
	     sr1 = new CountryListObject();
	     sr1.setName("AJMER");
	     sr1.setCityState("heena.mishra@in.patrika.com");
	     sr1.setPhone("9314409449");
	     results.add(sr1);
	     
	     sr1 = new CountryListObject();
	     sr1.setName("ALWAR");
	     sr1.setCityState("event.alwar@epatrika.com");
	     sr1.setPhone("9928088095");
	     results.add(sr1);
	     
	     sr1 = new CountryListObject();
	     sr1.setName("BIKANER");
	     sr1.setCityState("ravindra.harsh@in.patrika.com");
	     sr1.setPhone("9351205523 | 0151-3048311");
	     results.add(sr1);
	     
	     sr1 = new CountryListObject();
	     sr1.setName("SRI GANGANAGAR");
	     sr1.setCityState("sumesh.sharma@epatrika.com");
	     sr1.setPhone("9314409449 | 0154-3048237/49");
	     results.add(sr1);
	     
	     sr1 = new CountryListObject();
	     sr1.setName("KOTA");
	     sr1.setCityState("events.kota@epatrika.com");
	     sr1.setPhone("9928088095 | 0744-3045000");
	     results.add(sr1);
	     
	     sr1 = new CountryListObject();
	     sr1.setName("JODHPUR");
	     sr1.setCityState("liyakatjodhpur@gmail.com");
	     sr1.setPhone("9672727862 | 0291-3048110");
	     results.add(sr1);
	     
	     sr1 = new CountryListObject();
	     sr1.setName("UDAIPUR");
	     sr1.setCityState("sudhakar.event@gmail.com");
	     sr1.setPhone("7727866813");
	     results.add(sr1);
	     
	     return results;
	    }
	
}
